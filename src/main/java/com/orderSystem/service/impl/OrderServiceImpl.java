package com.orderSystem.service.impl;

import com.orderSystem.jobs.OrderCourier;
import com.orderSystem.repository.OrderRepository;
import com.orderSystem.model.Order;
import com.orderSystem.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }


    @Override
    public Optional<Order> findById(String id) { return orderRepository.findById(id);}

    @Override
    public Order findOrderById(String id) { return orderRepository.findOrderById(id);}

    @Override
    public int changeState(Order.State state, String id) {
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if(optionalOrder.isPresent()){
            Order order = optionalOrder.get();
            order.setState(state);
            orderRepository.save(order);
            System.out.println(order.getState());
            if(order.getState().equals(Order.State.PROCESSING) && OrderCourier.courierCounter < OrderCourier.maxCouriers ) {
                System.out.println(OrderCourier.courierCounter < OrderCourier.maxCouriers);
                if(OrderCourier.courierCounter <= OrderCourier.maxCouriers) {
                    OrderCourier courier = new OrderCourier(id, this);
                    OrderCourier.courierCounter++;
                    courier.start();
                    System.out.println("COURIER COUNTER = " + OrderCourier.courierCounter + "Max COURIER = " + OrderCourier.maxCouriers);
                    return 0;
                }
            }
            return -2;
        }
        return -1;
    }

    @Override
    public Order saveOrUpdateOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public void deleteById(String id) { orderRepository.deleteById(id); }

    @Override
    public List<Order> findByState(Order.State state) { return orderRepository.findByState(state); }

}
